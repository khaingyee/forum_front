Project Name: Forum-Front
This is the front-end of the Forum project built with Angular.

Scripts
In the project directory, you can run:
 *need node version 16.16 and above
ng serve
Runs the app in the development mode.<br />
Open http://localhost:4200 to view it in the browser.

ng build
Builds the app for production to the dist folder.

ng build --watch --configuration development
Builds the app for development to the dist folder and watches for changes.

ng test
Launches the test runner in the interactive watch mode.<br />

Dependencies
@angular/animations
@angular/common
@angular/compiler
@angular/core
@angular/forms
@angular/material
@angular/platform-browser
@angular/platform-browser-dynamic
@angular/router
@fortawesome/fontawesome-free
@fortawesome/free-solid-svg-icons
bootstrap
rxjs
tslib
zone.js
Dev Dependencies
@angular-devkit/build-angular
@angular/cli
@angular/compiler-cli
@types/jasmine
jasmine-core
karma
karma-chrome-launcher
karma-coverage
karma-jasmine
karma-jasmine-html-reporter
typescript
How to Use
Clone the repository.
Navigate to the root directory and run npm install to install the dependencies.
Run ng serve to start the development server.
Navigate to http://localhost:4200/ in your web browser to see the app running.