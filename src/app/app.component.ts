import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  loggedIn: boolean = false;
  username() {
    return localStorage.getItem('user')
    ? JSON.parse(localStorage.getItem('user') || '')['user']['name'] || null
    : null;
    }
  logout() {
    localStorage.clear();
  }
  constructor() { }

  // any shared logic or functions can be defined here
}
