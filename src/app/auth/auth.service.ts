import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiUrl = 'http://localhost:3000/api/v1/auth';

  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/login`, { email: username, password }).pipe(
      tap(data => localStorage.setItem('user', JSON.stringify(data)))
    );
  }

  register(name: string, email: string, password: string): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/register`, { name, email, password }).pipe(
      tap(data => localStorage.setItem('user', JSON.stringify(data)))
    );
  }

  logout(): void {
    localStorage.removeItem('user');
  }

  getUser(): any {
    return JSON.parse(localStorage.getItem('user') || '');
  }

  isLoggedIn(): boolean {
    return !!localStorage.getItem('user');
  }
}
