import { Component } from '@angular/core';
import { AuthService } from '../auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  public name: string = '';
  public email: string = '';
  public password: string = '';
  constructor(private auth: AuthService, private snackBar: MatSnackBar, private router: Router){};

  register() {
    this.auth.register(this.name, this.email, this.password).subscribe(
      (data) => {
        if (data) {
          this.snackBar.open('Success', 'Close', {
            duration: 3000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'snackbar-success' 
          });
          this.router.navigate(['/login']);
        } else {
          this.snackBar.open('Someting went wrong!', 'Close', {
            duration: 3000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'snackbar-error' 
          });
        }
      },
      (error) => {
        this.snackBar.open(error.error.message, 'Close', {
          duration: 3000, 
          horizontalPosition: 'center',
          verticalPosition: 'top',
          panelClass: 'snackbar-error' 
        });
      }
    );
  }
}
