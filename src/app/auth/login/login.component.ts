import { Component } from '@angular/core';
import { AuthService } from '../auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {
  public username: string = '';
  public password: string = '';
  constructor(private auth: AuthService, private snackBar: MatSnackBar, private router: Router){};
  login() {
    this.auth.login(this.username, this.password).subscribe(
      (data) => {
        if (data && data.success) {
          this.snackBar.open('Success', 'Close', {
            duration: 3000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'snackbar-success' 
          });
          this.router.navigate(['/forum']);
        } else {
          this.snackBar.open('Invalid username or password', 'Close', {
            duration: 3000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'snackbar-error' 
          });
        }
      },
      (error) => {
        this.snackBar.open('Invalid username or password', 'Close', {
          duration: 3000, 
          horizontalPosition: 'center',
          verticalPosition: 'top',
          panelClass: 'snackbar-error' 
        });
      }
    );
  }
  
}
