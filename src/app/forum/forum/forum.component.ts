import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ForumService } from '../forum.service';
import { Post, Comment } from './forum.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { faHeart } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.scss']
})

export class ForumComponent implements OnInit {
  image: any;
  file: File | undefined;
  showForumCreate = false;
  title: string = '';
  body: string = '';
  posts: Post[] = [];
  newComment = {body: ''};
  author: string = '';
  showComments: boolean = false;
  constructor(private service: ForumService, private snackBar: MatSnackBar, private router: Router){}
  
  ngOnInit(): void {
    this.loadPosts();
  }

  loadPosts() {
    this.service.getForums().subscribe((data) => {
      this.posts = data;
    });
  }

  toggleComments(post: Post): void {
    post.showComments = !post.showComments;
  }

  onFileSelected(event: any) {
    const file = event.target.files[0];
    if (file) {
      this.file = file;
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.image = reader.result;
      };
    }
  }

  createForum(): void {
    const formData = new FormData();
    formData.append('title', this.title);
    formData.append('body', this.body);
    formData.append('author', JSON.parse(localStorage.getItem('user') || '')['user']['_id']);
    if (this.file) {
      formData.append('image', this.file);
    }
    this.service.createForum(formData).subscribe(
      (data) => {
        if (data) {
          this.snackBar.open('Success created forum!', 'Close', {
            duration: 3000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'snackbar-success' 
          });
          this.showForumCreate = false;
          this.loadPosts();
        } else {
          this.snackBar.open('Something went wrong!', 'Close', {
            duration: 3000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'snackbar-error' 
          });
        }
      },
      (error) => {
        this.snackBar.open(error.error?.message, 'Close', {
          duration: 3000, 
          horizontalPosition: 'center',
          verticalPosition: 'top',
          panelClass: 'snackbar-error' 
        });
      }
    );
  }

  submitComment(post: any) {
    const data = {
      'content': this.newComment.body,
      'author': post.author._id,
      'post': post._id
    };
    this.service.createComment(data).subscribe(
      (data) => {
        if (data) {
          this.snackBar.open('Success created comment!', 'Close', {
            duration: 3000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'snackbar-success' 
          });
          this.loadPosts();
        } else {
          this.snackBar.open('Something went wrong!', 'Close', {
            duration: 3000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'snackbar-error' 
          });
        }
      },
      (error) => {
        this.snackBar.open(error.error?.message, 'Close', {
          duration: 3000, 
          horizontalPosition: 'center',
          verticalPosition: 'top',
          panelClass: 'snackbar-error' 
        });
      }, () => {
        this.newComment = {body: ''};
      }
    );
  }

  currentUser() {
    return localStorage.getItem('user')
    ? JSON.parse(localStorage.getItem('user') || '')['user']['_id'] || null
    : null;
  }
  likePost(post:any) {
    const data = {
      'author': this.currentUser(),
      'post': post._id,
      'isLike': (post.likes && post.likes.length > 0) ? !post.likes?.includes(this.currentUser()) : true
    };
    this.service.createLike(data).subscribe(
      (data) => {
        if (data) {
          this.snackBar.open('Success created comment!', 'Close', {
            duration: 3000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'snackbar-success' 
          });
          this.loadPosts();
        } else {
          this.snackBar.open('Something went wrong!', 'Close', {
            duration: 3000, 
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'snackbar-error' 
          });
        }
      },
      (error) => {
        this.snackBar.open(error.error?.message, 'Close', {
          duration: 3000, 
          horizontalPosition: 'center',
          verticalPosition: 'top',
          panelClass: 'snackbar-error' 
        });
      }
    );
  }
}
