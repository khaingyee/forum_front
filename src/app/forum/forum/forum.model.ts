export class Post {
    public _id: string;
    public title: string;
    public body: string;
    public author: Author;
    public image: string;
    public comments: Comment[];
    public likes!: String[];
    public createdAt: Date;
    public updatedAt: Date;
    public showComments: boolean;
  
    constructor(
      _id: string,
      title: string,
      body: string,
      author: Author,
      image: string,
      comments: Comment[],
      createdAt: Date,
      updatedAt: Date,
      showComments: boolean,
    ) {
      this._id = _id;
      this.title = title;
      this.body = body;
      this.author = author;
      this.image = image;
      this.comments = comments;
      this.createdAt = createdAt;
      this.updatedAt = updatedAt;
      this.showComments = showComments;
    }
  }
  
  export class Author {
    public _id: string;
    public name: string;
    public email: string;
  
    constructor(_id: string, name: string, email: string) {
      this._id = _id;
      this.name = name;
      this.email = email;
    }
  }
  
  export class Comment {
    public content: string;
    public author: Author;
    public post: string;
    public createdAt: Date;
  
    constructor(
      content: string,
      author: Author,
      post: string,
      createdAt: Date,
    ) {
      this.content = content;
      this.author = author;
      this.post = post;
      this.createdAt = createdAt;
    }
  }
  