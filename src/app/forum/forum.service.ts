import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ForumService {
  private apiUrl = 'http://localhost:3000/api/v1/forum';

  constructor(private http: HttpClient) {}

  createForum(formData: FormData): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('user') || '')['token']
    });
  
    return this.http.post<any>(`${this.apiUrl}/posts`, formData, { headers })
      .pipe(
        catchError(error => {
          console.error(error);
          return throwError('Something went wrong with the API request');
        })
      );
  }
  

  getForums(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/posts`)
             .pipe(
                catchError(error => {
                  console.error(error);
                  return throwError('Something went wrong with the API request');
                })
             );
  }

  createComment(data:any): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('user') || '')['token'],
    });
  
    return this.http.post<any>(`${this.apiUrl}/posts/comments`, data, { headers: headers })
      .pipe(
        catchError(error => {
          console.error(error);
          return throwError('Something went wrong with the API request');
        })
      );
  }

  createLike(data:any): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('user') || '')['token'],
    });
  
    return this.http.post<any>(`${this.apiUrl}/posts/like`, data, { headers: headers })
      .pipe(
        catchError(error => {
          console.error(error);
          return throwError('Something went wrong with the API request');
        })
      );
  }
}
