import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForumComponent } from './forum/forum.component';
import { CommentFormComponent } from './comment-form/comment-form.component';
import { FormsModule } from '@angular/forms';
import { ForumService } from './forum.service';
// import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    ForumComponent,
    CommentFormComponent
  ],
  imports: [
    CommonModule,
    // BrowserModule,
    FormsModule
  ],
  exports: [
    ForumComponent
  ],
  providers: [ForumService]
})
export class ForumModule { }
